package control;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;

import sfg_GUI.GUI;
import sfg_GUI.Link;

public class Controller {
	
	static Controller controller;
	static GUI gui;
	public static ArrayList<Link> links;
	public static int numberOfNodes;
	public static double[][] gainsArray;
	
	public static void main(String[] args) {
		controller = new Controller();
		gui = new GUI();
		links = new ArrayList<Link>();
	}
	
	public static void remove(Link link) {
		link.removeAll();
		link.revalidate();
		link.repaint();
		links.remove(link);
		gui.getFrame().remove(link);
	}
	
	public static void setOkBtnListener() {
		JButton okBtn = gui.getOkBtn();
		okBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				// Errors check
				if (!correctGraph()) {
					return;
				}
				createGainsArray();
				gui.setProcessingPanel();
				
				// solve the graph
				SFG signalFlowGraph = new SFG(gainsArray,gui.getStartNode() - 1, gui.getEndNode() - 1);
				if (signalFlowGraph.forwardPaths.isEmpty()) {
					gui.getErrorMessageLabel().setText("There is no forward paths from " + gui.getStartNode()+ " to " + gui.getEndNode());
					return;
				}
				signalFlowGraph.prepareResults();
				gui.forwardPathsTextArea.setText(signalFlowGraph.getForwardPathsResult());
				gui.loopsTextArea.setText(signalFlowGraph.getLoopsResult());
				gui.deltaTextArea.setText(signalFlowGraph.getDeltaResult());
				if (signalFlowGraph.transferFunction == 0.0) {
					gui.transferFunctionTextArea.setText("Transfer function for this set of inputs is undefined (i.e., Delta = 0");
				} else {
					gui.transferFunctionTextArea.setText(signalFlowGraph.getTransferFunctionResult());
				}
				
				// this is the last to be called
				gui.switchToProcessing();
				
			}
		});
	}
	
	private static boolean correctGraph() {
		// check if the start node and the end node are not the same node
		if (gui.getStartNode() == gui.getEndNode()) {
			gui.getErrorMessageLabel().setText("Start Node and End Node are the same.");
			return false;
		}
		// check if the gains are correct
		for (Link x : links) {
			if (x.getGain() == 0) {
				gui.getErrorMessageLabel().setText("Gains Can Not Equal Zero.");
				return false;
			}
		}
		// check if there are any repeated links
		for (int i = 0; i < links.size() - 1; i++) {
			Link x = links.get(i);
			for (int j = i + 1; j < links.size(); j++) {
				Link y = links.get(j);
				if (x.getFromNode() == y.getFromNode()) {
					if (x.getToNode() == y.getToNode()) {
						gui.getErrorMessageLabel().setText("You Can Not Add Repeated Links.");
						return false;
					}
				}
			}
		}
		// check if there is any node that doesn't have any linked node
		if (links.isEmpty()) {
			gui.getErrorMessageLabel().setText("Some Nodes Are Not Linked To Any Other Nodes.");
			return false;
		} else {
			for (int i = 1; i <= numberOfNodes; i++) {
				boolean linked = false;
				for (Link x : links) {
					if (i == x.getFromNode() || i == x.getToNode()) {
						linked = true;
						break;
					}
				}
				if (!linked) {
					gui.getErrorMessageLabel().setText("Some Nodes Are Not Linked To Any Other Nodes.");
					return false;
				}
			}
		}
		return true;
	}
	
	private static void createGainsArray() {
		gainsArray = new double[numberOfNodes][numberOfNodes];
		for (Link x : links) {
			int from = x.getFromNode();
			int to = x.getToNode();
			double gain = x.getGain();
			gainsArray[from - 1][to - 1] = gain;
		}
	}
}
