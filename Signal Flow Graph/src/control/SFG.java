package control;

import java.util.ArrayList;
import java.util.HashSet;

public class SFG {

	public ArrayList<Path> loops, forwardPaths;
	public ArrayList<ArrayList<ArrayList<Integer>>> nonTouchingLoops = new ArrayList<ArrayList<ArrayList<Integer>>>();
	public ArrayList<ArrayList<Integer>> combinations = new ArrayList<ArrayList<Integer>>();
	private double[][] gainsArray;
	private int startNode, endNode;
	private int[] visited;
	private ArrayList<Double> DeltaN;
	private double Delta;
	public double transferFunction;
	private StringBuilder FP, L, D, TF;

	public SFG(double[][] gainsArray, int startNode, int endNode) {
		this.gainsArray = gainsArray;
		this.startNode = startNode;
		this.endNode = endNode;
		visited = new int[gainsArray.length];
		loops = new ArrayList<Path>();
		forwardPaths = new ArrayList<Path>();
		DeltaN = new ArrayList<Double>();
		getPaths();
	}

	private void getPaths() {
		Path p = new Path();
		p.nodes.add(startNode);
		visited[startNode] = 1;

		DFS(startNode, p);

		// removing duplicate loops
		for (int i = 0; i < loops.size(); i++) {
			for (int j = i + 1; j < loops.size(); j++) {
				if (loops.get(i).nodes.equals((loops.get(j).nodes))) {
					loops.remove(j);
				}
			}
		}

		// getting gains
		getGains();
		
		// removing duplicate loops
		for (int i = 0; i < loops.size(); i++) {
			for (int j = i + 1; j < loops.size(); j++) {
				if (loops.get(i).gain == loops.get(j).gain) {
					HashSet<Integer> s = new HashSet<Integer>();
					s.addAll(loops.get(i).nodes);
					s.addAll(loops.get(j).nodes);
					if(s.size() == loops.get(i).nodes.size()-1) {
						loops.remove(j);
					}
				}
			}
		}

		getNontouchingLoops();
		getDeltaN();
		getDelta();
		getTransferFunction();
	}

	private void DFS(int start, Path p) {
		for (int i = 0; i < gainsArray.length; i++) {
			if (gainsArray[start][i] != 0) {
				if (visited[i] == 0) {
					p.nodes.add(i);
					if (p.nodes.get(p.nodes.size()-1) == endNode) {
						Path P = new Path();
						P.nodes = new ArrayList<Integer>(p.nodes);
						P.gain = p.gain;
						forwardPaths.add(P);
					}
					visited[i] = 1;
					DFS(i, p);
					visited[i] = 0;
					p.nodes.remove(p.nodes.size() - 1);
				} else {// loop
					p.nodes.add(i);
					Path temp = getLoopPath(p, i);
					if (!loops.contains(temp)) {
						loops.add(temp);
					}
					p.nodes.remove(p.nodes.size() - 1);
				}
			}
		}
	}

	@SuppressWarnings("unchecked")
	private Path getLoopPath(Path p, int end) {
		ArrayList<Integer> nodes = p.nodes;
		Path loop = new Path();
		loop.nodes.add(end);
		for (int i = nodes.size() - 2; i >= 0; i--) {
			loop.nodes.add(nodes.get(i));
			if (nodes.get(i) == end)
				break;
		}
		ArrayList<Integer> temp = (ArrayList<Integer>) loop.nodes.clone();
		loop.nodes.clear();
		for (int i = temp.size() - 1; i >= 0; i--) {
			loop.nodes.add(temp.get(i));
		}
		return loop;
	}

	private void getGains() {
		// forward paths
		for (int i = 0; i < forwardPaths.size(); i++) {
			Path p = forwardPaths.get(i);
			for (int j = 0; j < p.nodes.size() - 1; j++) {
				p.gain *= gainsArray[p.nodes.get(j)][p.nodes.get(j + 1)];
			}
		}
		// loops
		for (int i = 0; i < loops.size(); i++) {
			Path p = loops.get(i);
			for (int j = 0; j < p.nodes.size() - 1; j++) {
				p.gain *= gainsArray[p.nodes.get(j)][p.nodes.get(j + 1)];
			}
		}
	}
	
	private void combinationUtil(int arr[], int data[], int start,
            int end, int index, int r) {
		if (index == r) {
			ArrayList<Integer> x = new ArrayList<Integer>();
			for (int j=0; j<r; j++) {
				x.add(data[j]);
			}
			combinations.add(x);
			return;
		}
		for (int i=start; i<=end && end-i+1 >= r-index; i++) {
			data[index] = arr[i];
			combinationUtil(arr, data, i+1, end, index+1, r);
		}
	}
	
	private void getCombination(int arr[], int n, int r) {
		combinations.clear();
		int data[]=new int[r];
		combinationUtil(arr, data, 0, n-1, 0, r);
	}

	private void getNontouchingLoops() {
		int[] arr = new int[loops.size()];
		for (int i = 0; i < loops.size(); i++) {
			arr[i] = i;
		}
		int loopsCounter = 2;
		int index = 0;
		while (true) {
			nonTouchingLoops.add(new ArrayList<ArrayList<Integer>>());
			getCombination(arr, arr.length, loopsCounter);
			for (ArrayList<Integer> combination : combinations) {
				ArrayList<Path> paths = new ArrayList<Path>();
				for (int j = 0; j < combination.size(); j++) {
					paths.add(loops.get(combination.get(j)));
				}
				boolean touching = false;
				for (int j = 0; j < paths.size() - 1; j++) {
					Path x = paths.get(j);
					for (int k = j + 1; k < paths.size(); k++) {
						Path y = paths.get(k);
						for (int z = 0; z < x.nodes.size(); z++) {
							if (y.nodes.contains(x.nodes.get(z))) {
								touching = true;
								break;
							}
						}
						if (touching) {
							break;
						}
					}
				}
				if (!touching) {
					nonTouchingLoops.get(index).add(combination);
				}
			}
			if (nonTouchingLoops.get(index).isEmpty()) {
				return;
			}
			index++;
			loopsCounter++;
		}
	}

	private void getDeltaN() {
		for (int i = 0; i < forwardPaths.size(); i++) {
			Path forwardPath = forwardPaths.get(i);
			double loopsGain = 0;
			for (int j = 0; j < loops.size(); j++) {
				Path loop = loops.get(j);
				boolean touching = false;
				for (int k = 0; k < loop.nodes.size(); k++) {
					if (forwardPath.nodes.contains(loop.nodes.get(k))) {
						touching = true;
						break;
					}
				}
				if (!touching) {
					loopsGain += loop.gain;
					touching = false;
				}
			}
			DeltaN.add(1 - loopsGain);
		}
	}

	private void getDelta() {
		double sumOfGainOfAllSingleLoops = 0;
		double sumOfNonTouching = 0.0;
		for (int i = 0; i < loops.size(); i++) {
			sumOfGainOfAllSingleLoops += loops.get(i).gain;
		}
		int sign = 1;
		for (ArrayList<ArrayList<Integer>> nonTouching : nonTouchingLoops) {
			double weight = 0.0;
			for (ArrayList<Integer> x : nonTouching) {
				double w = 1.0;
				for (Integer z : x) {
					Path path = loops.get(z);
					w *= path.gain;
				}
				weight += w;
			}
			sumOfNonTouching = sumOfNonTouching + sign * weight;
			sign = -sign;
		}
		Delta = 1 - sumOfGainOfAllSingleLoops + sumOfNonTouching;
	}
	
	private void getTransferFunction() {
		double numerator = 0;
		for(int i = 0 ;i<forwardPaths.size();i++) {
			numerator += forwardPaths.get(i).gain * DeltaN.get(i);
		}
		if(Delta != 0)
			transferFunction = numerator/Delta;
		else
			transferFunction = 0;
		
	}

	public void prepareResults() {
		// forward paths result
		FP = new StringBuilder();
		for (int j = 0; j < forwardPaths.size(); j++) {
			ArrayList<Integer> nodes = forwardPaths.get(j).nodes;
			for (int k = 0; k < nodes.size(); k++) {
				nodes.set(k, nodes.get(k) + 1);
			}
			FP.append(String.valueOf(nodes).replace(", ", "-"));
			FP.append(" Gain = " + forwardPaths.get(j).gain);
			FP.append("\n");
		}
		
		// single loops result
		L = new StringBuilder();
		if (loops.isEmpty()) {
			L.append("None");
		} else {
			L.append("Loops\n");
			for (int j = 0; j < loops.size(); j++) {
				ArrayList<Integer> nodes = loops.get(j).nodes;
				for (int k = 0; k < nodes.size(); k++) {
					nodes.set(k, nodes.get(k) + 1);
				}
				L.append(String.valueOf(nodes).replace(", ", "-"));
				L.append(" Gain = " + loops.get(j).gain);
				L.append("\n");
			}
			
			// non touching loops result
			int counter = 2;
			for (ArrayList<ArrayList<Integer>> nonTouching : nonTouchingLoops) {
				L.append("\n");
				L.append(counter + " Nontouching Loops\n");
				if (nonTouching.isEmpty()) {
					L.append("None");
					break;
				} else {
					for (ArrayList<Integer> x : nonTouching) {
						for (int i = 0; i < x.size(); i++) {
							Path p = loops.get(x.get(i));
							L.append(String.valueOf(p.nodes).replace(", ", "-"));
							L.append(" Gain = " + p.gain);
							if (i != x.size() - 1) {
								L.append(" / ");
							}
						}
						L.append("\n");
					}
					counter++;
				}
			}
		}
		
		// delta result
		D = new StringBuilder();
		D.append("Delta = " + Delta + "\n");
		for (int j = 0; j < DeltaN.size(); j++) {
			D.append("\n");
			D.append("Delta("+(j + 1)+") = " + DeltaN.get(j));
		}
		
		// transfer function result
		TF = new StringBuilder();
		TF.append("From node " + (startNode + 1) + " to node " + (endNode + 1) + "  Y(s) / R(s) = " + transferFunction);
	}
	
	public String getForwardPathsResult() {
		return FP.toString();
	}
	
	public String getLoopsResult() {
		return L.toString();
	}
	
	public String getDeltaResult() {
		return D.toString();
	}
	
	public String getTransferFunctionResult() {
		return TF.toString();
	}
}
