package test;

import static org.junit.jupiter.api.Assertions.*;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;

import control.SFG;

class SFG_testing {

	@Test
	void test1() {
		double[][] gainsArray = new double[7][7];
		gainsArray[0][1] = 1;
		gainsArray[1][2] = 5;
		gainsArray[2][3] = 10;
		gainsArray[3][4] = 2;
		gainsArray[4][5] = 1;
		gainsArray[1][6] = 10;
		gainsArray[6][4] = 2;
		gainsArray[4][3] = -2;
		gainsArray[3][2] = -1;
		gainsArray[4][1] = -1;
		gainsArray[6][6] = -1;
		SFG sfg = new SFG(gainsArray,0,5);
		sfg.prepareResults();
		double expected = 0.9333, actual = truncateDecimal(sfg.transferFunction, 4);
		assertEquals(expected, actual);
	}
	
	@Test
	void test2() {
		double[][] gainsArray = new double[10][10];
		gainsArray[0][1] = 1;
		gainsArray[1][2] = 5;
		gainsArray[2][3] = 2.3;
		gainsArray[3][4] = 1;
		gainsArray[4][8] = 5.5;
		gainsArray[8][9] = 1;
		gainsArray[3][2] = -4;
		gainsArray[4][3] = -7.3;
		gainsArray[1][5] = 2;
		gainsArray[5][6] = 2.4;
		gainsArray[6][7] = 5.4;
		gainsArray[7][8] = 1.3;
		gainsArray[7][6] = -0.01;
		gainsArray[6][5] = -3.1;
		SFG sfg = new SFG(gainsArray,0,9);
		double expected = 7.5813, actual = truncateDecimal(sfg.transferFunction, 4);
		assertEquals(expected, actual);
	}
	
	private static double truncateDecimal(double x,int numberofDecimals) {
	    if (x > 0) {
	        return (new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_FLOOR)).doubleValue();
	    } else {
	        return (new BigDecimal(String.valueOf(x)).setScale(numberofDecimals, BigDecimal.ROUND_CEILING)).doubleValue();
	    }
	}

}
