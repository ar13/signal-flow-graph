package sfg_GUI;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.util.ArrayList;

public class Drawer {
	
	private int numberOfNodes;
	private ArrayList<Link> links;
	final int startNode, endNode;
	
	public Drawer(final int numberOfNodes, final ArrayList<Link> links, final int startNode, final int endNode) {
		this.numberOfNodes = numberOfNodes;
		this.links = links;
		this.startNode = startNode;
		this.endNode = endNode;
	}
	
	public void drawNodes(Graphics g) {
		g.setFont(new Font("Calibri", Font.BOLD, 12));
		final int x = 40, y = 1000, radius = 20;
		for (int i = 0; i < numberOfNodes; i++) {
			g.setColor(Color.WHITE);
			if (i + 1 == startNode) {
				g.setColor(Color.GREEN);
			} else if (i + 1 == endNode) {
				g.setColor(Color.YELLOW);
			}
			g.fillOval(x - radius + i * 120, y - radius, radius * 2, radius * 2);
			g.setColor(Color.BLACK);
			g.drawString(String.valueOf(i + 1), x - 5 + i * 120, y + 5);
		}
	}
	
	public void drawLinks(Graphics g) {
		g.setFont(new Font("Calibri", Font.BOLD, 12));
		g.setColor(Color.WHITE);
		final int y = 1000;
		for (Link link : links) {
			// drawing the link
			int x1 = 40 + 120 * (link.getFromNode() - 1);
			int x2 = 40 + 120 * (link.getToNode() - 1);
			int width = Math.abs(x2 - x1);
			int height = Math.abs(x2 - x1) / 2;//20 * Math.abs(link.getFromNode() - link.getToNode()) + 160;
			
			int startAngle = 0;
			int endAngle = 180;
			if (link.getFromNode() > link.getToNode()) {
				endAngle = -180;
			}
			g.drawArc(Math.min(x1, x2), y - (height / 2), width, height, startAngle, endAngle);
			
			// drawing the arrow head
			int arrowMidX = Math.min(x1, x2) + width / 2;
			int arrowMidY = y - (height / 2);
			int[] arrowX = new int[3];
			int[] arrowY = new int[3];
			if (link.getFromNode() < link.getToNode()) {
				arrowX[0] = arrowMidX - 10;
				arrowY[0] = arrowMidY - 5;
				arrowX[1] = arrowMidX - 10;
				arrowY[1] = arrowMidY + 5;
				arrowX[2] = arrowMidX + 10;
				arrowY[2] = arrowMidY;
			} else {
				arrowX[0] = arrowMidX - 10;
				arrowY[0] = arrowMidY + height;
				arrowX[1] = arrowMidX + 10;
				arrowY[1] = arrowMidY - 5 + height;
				arrowX[2] = arrowMidX + 10;
				arrowY[2] = arrowMidY + 5 + height;
			}
			g.fillPolygon(arrowX, arrowY, 3);
			
			// drawing the gains
			int gainX = arrowX[0], gainY;
			gainY = (link.getFromNode() < link.getToNode())? arrowY[0] - 5 : arrowY[2] + 10;
			double gain = link.getGain();
			g.drawString(String.valueOf(gain), gainX, gainY);
		}
	}
}
