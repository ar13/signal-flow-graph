package sfg_GUI;

import java.awt.Adjustable;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Rectangle;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.AdjustmentEvent;
import java.awt.event.AdjustmentListener;

import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.SwingConstants;
import javax.swing.border.LineBorder;

import control.Controller;

public class GUI {

	private JFrame frame;
	private JPanel displayPanel;
	private JPanel mainMenu, addLinks;
	private JLabel enterNumberLbl;
	private JButton enterBtn;
	private JTextField numOfNodesTxtField;
	private JLabel messageLbl;
	private JButton addNewLink;
	private JPanel addLinksBackground;
	private JScrollPane scrollPane;
	private JButton okBtn, backBtn;
	private int numberOfLinks = 0;
	private JComboBox<Integer> startNode, endNode;
	private JLabel SN, EN;
	private JLabel errorMsg;
	private JPanel processingPanel;
	private JPanel drawingArea;
	public JTextArea forwardPathsTextArea, loopsTextArea, deltaTextArea, transferFunctionTextArea;
	private Drawer drawer;
	
	public JFrame getFrame() {
		return frame;
	}

	/**
	 * Create the application.
	 */
	public GUI() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frame = new JFrame();
		frame.setBounds(100, 100, 300, 200);
		frame.getContentPane().setLayout(new BorderLayout());
		setDisplayPanel();
		setMainMenu();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setResizable(false);
		frame.setTitle("Signal Flow Graph");
		frame.setVisible(true);
	}
	
	private void setDisplayPanel() {
		displayPanel = new JPanel();
		displayPanel.setLayout(new CardLayout());
		frame.getContentPane().add(displayPanel, BorderLayout.CENTER);
	}
	
	private void setMainMenu() {
		mainMenu = new JPanel(null);
		mainMenu.setBackground(new Color(35, 43, 62));
		displayPanel.add("mainMenu", mainMenu);
		
		enterNumberLbl = new JLabel("Enter number of nodes");
		enterNumberLbl.setHorizontalAlignment(SwingConstants.CENTER);
		enterNumberLbl.setForeground(Color.WHITE);
		Rectangle enterRec = new Rectangle(130, 14);
		enterNumberLbl.setBounds((frame.getWidth() - (int)enterRec.getWidth()) / 2,
									43, (int)enterRec.getWidth(), (int)enterRec.getHeight());
		
		numOfNodesTxtField = new JTextField();
		numOfNodesTxtField.setBounds(85, 68, 53, 20);
		numOfNodesTxtField.setHorizontalAlignment(SwingConstants.CENTER);
		mainMenu.add(numOfNodesTxtField);
		
		mainMenu.add(enterNumberLbl);
		setEnterBtn();
		setMessageLabel();
	}
	
	private void setEnterBtn() {
		enterBtn = new JButton("Enter");
		enterBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				boolean correctInput = true;
				try {
					Controller.numberOfNodes = Integer.parseInt(numOfNodesTxtField.getText());
					if (Controller.numberOfNodes < 2 || Controller.numberOfNodes > 99) {
						correctInput = false;
					}
				} catch(NumberFormatException nfe) {
					correctInput = false;
				}
				if (correctInput) {
					switchToAddLinksFromMainMenu();
				} else {
					messageLbl.setText("Number of nodes must be an integer between 2 and 99");
				}
			}
		});
		enterBtn.setBounds(151, 67, 64, 23);
		enterBtn.setBackground(new Color(53, 63, 89));
		enterBtn.setForeground(Color.WHITE);
		enterBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		enterBtn.setFocusPainted(false);
		mainMenu.add(enterBtn);
	}
	
	private void setAddLinks() {
		addLinks = new JPanel();
		addLinks.setLayout(null);
		addLinks.setBackground(new Color(35, 43, 62));
		addLinksBackground = new JPanel(new BorderLayout());
		scrollPane = new JScrollPane(addLinks);
		scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.getVerticalScrollBar().setUnitIncrement(50);
		addLinksBackground.add(scrollPane, BorderLayout.CENTER);
		addLinks.setBounds(0, 0, addLinksBackground.getWidth() - scrollPane.getVerticalScrollBar().getWidth(), 120);
		addLinks.setPreferredSize(new Dimension(addLinksBackground.getWidth() - scrollPane.getVerticalScrollBar().getWidth(), 120));
		
		displayPanel.add("addLinksBackground", addLinksBackground);
		
		setAddNewLinkBtn();
		setOkBtn();
		setBackBtn();
		setErrorMessageLabel();
		setStartNodeBox();
		setEndNodeBox();
	}
	
	private void setMessageLabel() {
		messageLbl = new JLabel();
		messageLbl.setFont(new Font("Tahoma", Font.PLAIN, 11));
		messageLbl.setHorizontalAlignment(SwingConstants.CENTER);
		messageLbl.setForeground(Color.RED);
		messageLbl.setBounds(10, 101, 274, 14);
		mainMenu.add(messageLbl);
	}
	
	private void setAddNewLinkBtn() {
		addNewLink = new JButton("+ Add a new link");
		addNewLink.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				numberOfLinks++;
				addLinks.add(newLinkPanel());
				addNewLink.setLocation(0, addNewLink.getY() + addNewLink.getHeight());
				addLinks.setSize(addLinks.getWidth(), 120 + numberOfLinks * 30);
				addLinks.setPreferredSize(new Dimension(addLinks.getWidth(), 120 + numberOfLinks * 30));
				EN.setLocation(EN.getX(), 43 + numberOfLinks * 30);
				SN.setLocation(SN.getX(), 43 + numberOfLinks * 30);
				startNode.setLocation(startNode.getX(), 40 + numberOfLinks * 30);
				endNode.setLocation(endNode.getX(), 40 + numberOfLinks * 30);
				okBtn.setLocation(150, 80 + numberOfLinks * 30);
				backBtn.setLocation(40, 80 + numberOfLinks * 30);
				errorMsg.setLocation(2, 100 + numberOfLinks * 30);
				JScrollBar sc = scrollPane.getVerticalScrollBar();
				AdjustmentListener downScroller = new AdjustmentListener() {
					@Override
					public void adjustmentValueChanged(AdjustmentEvent e) {
						Adjustable adjustable = e.getAdjustable();
						adjustable.setValue(adjustable.getMaximum());
						sc.removeAdjustmentListener(this);
					}
				};
				sc.addAdjustmentListener(downScroller);
			}
		});
		addNewLink.setBounds(0, 0, 276, 30);
		addNewLink.setFocusPainted(false);
		addNewLink.setForeground(Color.WHITE);
		addNewLink.setBackground(new Color(36, 102, 255));
		addNewLink.setCursor(new Cursor(Cursor.HAND_CURSOR));
		addLinks.add(addNewLink);
	}
	
	private JPanel newLinkPanel() {
		Link link = new Link(Controller.numberOfNodes);
		Controller.links.add(link);
		if (numberOfLinks % 2 == 1) {
			link.setBackground(new Color(35, 43, 62));
		} else {
			link.setBackground(new Color(53, 63, 89));
		}
		link.setBounds(0, addNewLink.getY(), addNewLink.getWidth(), addNewLink.getHeight());
		link.setBorder(new LineBorder(Color.BLACK, 1));
		return link;
	}
	
	private void setStartNodeBox() {
		SN = new JLabel("Start Node");
		SN.setForeground(Color.WHITE);
		SN.setSize(100, 14);
		SN.setLocation(10, 43);
		addLinks.add(SN);
		startNode = new JComboBox<Integer>();
		addNodesToBox(startNode);
		startNode.setSize(45, 20);
		startNode.setLocation(80, 40);
		addLinks.add(startNode);
	}
	
	private void setEndNodeBox() {
		EN = new JLabel("End Node");
		EN.setForeground(Color.WHITE);
		EN.setSize(100, 14);
		EN.setLocation(140, 43);
		addLinks.add(EN);
		endNode = new JComboBox<Integer>();
		addNodesToBox(endNode);
		endNode.setSize(45, 20);
		endNode.setLocation(200, 40);
		addLinks.add(endNode);
	}
	
	private void setBackBtn() {
		backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				Controller.links.clear();
				messageLbl.setText("");
				switchToMainMenu();
			}
		});
		backBtn.setForeground(Color.WHITE);
		backBtn.setBackground(new Color(53, 63, 89));
		backBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		backBtn.setFocusPainted(false);
		backBtn.setSize(83, 20);
		backBtn.setLocation(40, 80);
		addLinks.add(backBtn);
	}
	
	private void setOkBtn() {
		okBtn = new JButton("OK");
		okBtn.setForeground(Color.WHITE);
		okBtn.setBackground(new Color(53, 63, 89));
		okBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		okBtn.setFocusPainted(false);
		okBtn.setSize(83, 20);
		okBtn.setLocation(150, 80);
		Controller.setOkBtnListener();
		addLinks.add(okBtn);
	}
	
	public JButton getOkBtn() {
		return okBtn;
	}
	
	private void setErrorMessageLabel() {
		errorMsg = new JLabel("");
		errorMsg.setFont(new Font(errorMsg.getFont().getName(), Font.PLAIN, 10));
		errorMsg.setHorizontalAlignment(SwingConstants.CENTER);
		errorMsg.setForeground(Color.RED);
		errorMsg.setSize(270, 20);
		errorMsg.setLocation(2, 100);
		addLinks.add(errorMsg);
	}
	
	public JLabel getErrorMessageLabel() {
		return errorMsg;
	}
	
	public void setProcessingPanel() {
		processingPanel = new JPanel(null);
		processingPanel.setBackground(new Color(35, 43, 62));
		displayPanel.add("processingPanel", processingPanel);
		
		JPanel backPanel = new JPanel(new BorderLayout());
		backPanel.setBounds(5, 5, 990, 660);
		drawer = new Drawer(Controller.numberOfNodes, Controller.links, getStartNode(), getEndNode());
		drawingArea = new JPanel(new BorderLayout()) {
			private static final long serialVersionUID = 1L;
			@Override
			public void paintComponent(Graphics g){
				super.paintComponent(g);
				drawer.drawLinks(g);
				drawer.drawNodes(g);
				
				repaint();
			}
		};
		drawingArea.setBackground(new Color(53, 63, 89));
		drawingArea.setPreferredSize(new Dimension(12000, 2000));
		JScrollPane scrollPane = new JScrollPane(drawingArea);
		scrollPane.getVerticalScrollBar().setUnitIncrement(20);
		scrollPane.getVerticalScrollBar().setMaximum(2000);
		scrollPane.getVerticalScrollBar().setValue(650);
		backPanel.add(scrollPane);
		
		JButton backBtn = new JButton("Back");
		backBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				switchToAddLinksFromProcessingPanel();
			}
		});
		backBtn.setForeground(Color.WHITE);
		backBtn.setBackground(new Color(53, 63, 89));
		backBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		backBtn.setFocusPainted(false);
		backBtn.setSize(83, 20);
		backBtn.setLocation(1010, 5);
		processingPanel.add(backBtn);
		
		processingPanel.add(backPanel);
		setTextAreas();
	}
	
	private void setTextAreas() {
		JLabel fp, l, d, tf;
		fp = new JLabel("Forward Paths");
		fp.setFont(new Font("Calibri", Font.BOLD, 12));
		fp.setForeground(Color.WHITE);
		fp.setLocation(1010, 36);
		fp.setSize(100, 30);
		processingPanel.add(fp);
		forwardPathsTextArea = new JTextArea();
		forwardPathsTextArea.setFont(new Font("Calibri", Font.BOLD, 16));
		JScrollPane sp1 = new JScrollPane(forwardPathsTextArea);
		sp1.setBounds(1010, 62, 279, 150);
		processingPanel.add(sp1);
		
		
		l = new JLabel("Loops");
		l.setFont(new Font("Calibri", Font.BOLD, 12));
		l.setForeground(Color.WHITE);
		l.setLocation(1010, 200);
		l.setSize(100, 80);
		processingPanel.add(l);
		loopsTextArea = new JTextArea();
		loopsTextArea.setFont(new Font("Calibri", Font.BOLD, 16));
		JScrollPane sp2 = new JScrollPane(loopsTextArea);
		sp2.setBounds(1010, 249, 279, 150);
		processingPanel.add(sp2);
		
		d = new JLabel("Delta");
		d.setFont(new Font("Calibri", Font.BOLD, 12));
		d.setForeground(Color.WHITE);
		d.setLocation(1010, 390);
		d.setSize(100, 80);
		processingPanel.add(d);
		deltaTextArea = new JTextArea();
		deltaTextArea.setFont(new Font("Calibri", Font.BOLD, 16));
		JScrollPane sp3 = new JScrollPane(deltaTextArea);
		sp3.setBounds(1010, 440, 279, 140);
		processingPanel.add(sp3);
		
		tf = new JLabel("Transfer Function");
		tf.setFont(new Font("Calibri", Font.BOLD, 12));
		tf.setForeground(Color.WHITE);
		tf.setLocation(1010, 570);
		tf.setSize(100, 80);
		processingPanel.add(tf);
		transferFunctionTextArea = new JTextArea();
		transferFunctionTextArea.setFont(new Font("Calibri", Font.BOLD, 16));
		JScrollPane sp4 = new JScrollPane(transferFunctionTextArea);
		sp4.setBounds(1010, 620, 279, 45);
		processingPanel.add(sp4);
	}
	
	/*
	 * switch between cards
	 */
	public void switchToMainMenu() {
		frame.setBounds(frame.getX(), frame.getY(), 300, 200);
		CardLayout c = (CardLayout)(displayPanel.getLayout());
		c.show(displayPanel, "mainMenu");
	}
	
	public void switchToAddLinksFromMainMenu() {
		setAddLinks();
		numberOfLinks = 0;
		frame.setBounds(frame.getX(), frame.getY(), 300, 400);
		CardLayout c = (CardLayout)(displayPanel.getLayout());
		c.show(displayPanel, "addLinksBackground");
	}
	
	public void switchToAddLinksFromProcessingPanel() {
		frame.setBounds(frame.getX(), frame.getY(), 300, 400);
		CardLayout c = (CardLayout)(displayPanel.getLayout());
		c.show(displayPanel, "addLinksBackground");
	}
	
	public void switchToProcessing() {
		frame.setBounds(frame.getX(), frame.getY(), 1300, 700);
		CardLayout c = (CardLayout)(displayPanel.getLayout());
		c.show(displayPanel, "processingPanel");
	}
	
	private void addNodesToBox(JComboBox<Integer> cb) {
		for (int i = 1; i <= Controller.numberOfNodes; i++) {
			cb.addItem(i);
		}
	}
	
	public int getStartNode() {
		return (int) startNode.getSelectedItem();
	}
	
	public int getEndNode() {
		return (int) endNode.getSelectedItem();
	}
}
