package sfg_GUI;

import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

import control.Controller;

import javax.swing.JComboBox;
import javax.swing.JTextField;
import java.awt.Color;
import java.awt.Cursor;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.ImageIcon;
import javax.swing.JButton;

public class Link extends JPanel {
	
	private static final long serialVersionUID = 1L;
	private final int numberOfNodes;
	private JComboBox<Integer> fromNodeBox, toNodeBox;
	private JTextField gainTxt;
	
	public Link(final int numberOfNodes) {
		this.numberOfNodes = numberOfNodes;
		setLayout(null);
		
		JLabel fromLabel = new JLabel("From");
		fromLabel.setForeground(Color.WHITE);
		fromLabel.setHorizontalAlignment(SwingConstants.CENTER);
		fromLabel.setBounds(2, 11, 35, 14);
		add(fromLabel);
		
		JLabel toLabel = new JLabel("To");
		toLabel.setForeground(Color.WHITE);
		toLabel.setHorizontalAlignment(SwingConstants.CENTER);
		toLabel.setBounds(80, 11, 35, 14);
		add(toLabel);
		
		JLabel gainLabel = new JLabel("Gain");
		gainLabel.setForeground(Color.WHITE);
		gainLabel.setHorizontalAlignment(SwingConstants.CENTER);
		gainLabel.setBounds(158, 11, 35, 14);
		add(gainLabel);
		
		setFromNodeBox();
		setToNodeBox();
		setGainTxt();
		setRemoveBtn();
		
		addNodesToBox(fromNodeBox);
		addNodesToBox(toNodeBox);
	}
	
	private void setRemoveBtn() {
		JButton removeBtn = new JButton();
		removeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent AE) {
				Controller.remove(Link.this);
			}
		});
		removeBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
		removeBtn.setIcon(new ImageIcon("./icons/removeBtn.png"));
		removeBtn.setBounds(246, 8, 20, 20);
		add(removeBtn);
	}

	private void setFromNodeBox() {
		fromNodeBox = new JComboBox<Integer>();
		fromNodeBox.setBounds(36, 8, 45, 20);
		add(fromNodeBox);
	}
	
	private void setToNodeBox() {
		toNodeBox = new JComboBox<Integer>();
		toNodeBox.setBounds(106, 8, 45, 20);
		add(toNodeBox);
	}
	
	private void setGainTxt() {
		gainTxt = new JTextField();
		gainTxt.setHorizontalAlignment(SwingConstants.CENTER);
		gainTxt.setBounds(191, 8, 40, 20);
		add(gainTxt);
	}
	
	private void addNodesToBox(JComboBox<Integer> cb) {
		for (int i = 1; i <= numberOfNodes; i++) {
			cb.addItem(i);
		}
	}
	
	/*
	 * if gain is incorrect this method returns 0, otherwise it returns the gain.
	 */
	public double getGain() {
		double g;
		try {
			g = Double.parseDouble(gainTxt.getText());
		} catch(NumberFormatException nfe) {
			g = 0.0;
		}
		return g;
	}
	
	public int getFromNode() {
		return (int) fromNodeBox.getSelectedItem();
	}
	
	public int getToNode() {
		return (int) toNodeBox.getSelectedItem();
	}
}
